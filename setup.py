#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


setup(name="pyLoro",
      version="0.1",
      description="Envio de Mensajes a Moviles",
      author="Carlos Alberto Garcia Diaz",
      author_email="foxcarlos@gmail",
      url="http://foxcarlos.wordpress.com",
      license="GPL",
      scripts=["frmenviar2.py"],
      packages = find_packages(),
      install_requires = ["tweepy"],
)     
