#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time
from daemon import runner
import os
import sys
import zmq
import ConfigParser
import android
import urllib
import urllib2

class smsResponder():
    def __init__(self):

        self.nombreArchivoConf = 'pyloro.cfg'
        self.fc = ConfigParser.ConfigParser()

        #Propiedades de la Clase
        self.archivoLog = ''
        self.tiempo = 10

        self.configInicial()
        self.configDemonio()

    def configInicial(self):
        '''Metodo que permite extraer todos los parametros
        del archivo de configuracion pyloro.cfg que se
        utilizara en todo el script'''

        #Obtiene Informacion del archivo de Configuracion .cfg
        self.ruta_arch_conf = os.path.dirname(sys.argv[0])
        self.archivo_configuracion = os.path.join(self.ruta_arch_conf, self.nombreArchivoConf)
        self.fc.read(self.archivo_configuracion)

        #Obtiene el nombre del archivo .log para uso del Logging
        seccion = 'RUTAS'
        opcion = 'archivo_log'
        self.archivoLog = self.fc.get(seccion, opcion)
        self.responder = self.fc.get('MENSAJES', 'responder')

    def configDemonio(self):
        '''Configuiracion del Demonio'''

        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path =  '/tmp/demonioResponder.pid'
        self.pidfile_timeout = 5

    def configLog(self):
        '''Metodo que configura los Logs de error tanto el nombre
        del archivo como su ubicacion asi como tambien los
        metodos y formato de salida'''

        #Extrae de la clase la propiedad que contiene el nombre del archivo log
        nombreArchivoLog = self.archivoLog
        self.logger = logging.getLogger("DemonioResponder")
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter("%(levelname)s--> %(asctime)s - %(name)s:  %(message)s", datefmt='%d/%m/%Y %I:%M:%S %p')
        handler = logging.FileHandler(nombreArchivoLog)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        return handler

    def enviarWebService(self, numero, mensaje):
        ''' parametros 2, (string:NumeroTelefono, string:MensajeSMS)
        Metodo que permite enviar via web service a pyLoroWeb un SMS
        a cualquier telefono movil
        '''

        print(numero, mensaje)
        url = 'http://10.121.6.12/mensaje'
        data = urllib.urlencode({'numero' : numero, 'mensaje'  : mensaje})
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        respuesta = response.read()
        return respuesta

    def enviarSocket(self, registros):
        '''Parametros 2: (tupla, string):
        (Registros, "String:Nombre del Campo de la Tabla que se actualizara")

        Este Metodo recorre una lista de las personas que se le enviara el
        mensaje SMS de Respuesta para luego enviarla via ip por Socket a
        (el o los) Servidores serverZMQ
        NOTA: Cuando se envia al ServidorZMQ este devuelve mediante self.socket.recv()
        ('1') si todo salio bien o ('0') en caso de fallar'''


        '''Algunos telefono generan error luego de enviar mas de 98 sms
        por tal motivo solo se permitira enviar 98 SMS por cada telefono
        Servidor que este ecuchando es decir por cada demonioServidor.py
        que se este ejecutando, esto se sabe mirando el archivo .cfg
        y viendo cuantos servidores aparecen en la seccion SERVER_ZQM_DEMONIOS'''

        #cantDemonioServ = len(self.fc.items('SERVER_ZQM_DEMONIOS'))
        #totalRegEnviar = 98 * cantDemonioServ

        for fila in registros:
            id, numero, sms = fila
            #mensaje = self.responder

            msg = '{0}^{1}'.format(numero, sms)

            #Se extrae y muestra en el .log solo una parte del mensaje
            self.logger.info(msg[:100])

            """
            Ahora se utilizara el webservice de pyLoroWeb para el envio de los SMS
            #Se enviar al servidor ZMQ para que envie el SMS
            self.socket.send(msg)

            #Se recibe un valor que verifica si llego bien el SMS
            # '1' si todo salio bien o '0' si no se pudo enviar el SMS
            msg_in = self.socket.recv()
            noEnviado, nombreServidor = msg_in.split(',')
            """

            #Aqui se inserta el web services
            noEnviado = self.enviarWebService(numero, sms)
            nombreServidor = ''  # Antes el nombre del servidor lo devolvia self.socket.recv() ahora no

            if int(noEnviado):
                #Si se logro enviar el SMS, se marca el Mensaje como Leido
                self.droid.smsMarkMessageRead([id], True)
            else:
                self.logger.warn('Houston Tenemos un Problema con el Telefono:{0},\
                    no se pudo enviar el SMS desde el Servidor {1}'.format(numero, nombreServidor))

    def smsProcesar(self, registros):
        '''Este metodo recibe una lista que se tomo de la bandeja de entrada
        de los telefonos y toma el id, el numero de telef y el mensaje y los
        organiza bien en una lista para luego proceder a enviar los mensajes'''

        listaEnviar = []
        for fila in registros:
            id, numero, cuerpo = fila
            cuerpo =  cuerpo if cuerpo else 'vacio'
            numero =  numero if numero else 'sinNumero'
            mensaje = self.responder

            '''
            #Eliminar espacios que estan demas del mensajes
            separarCuerpo = cuerpo.strip().upper().split(' ')
            contarEspacios = separarCuerpo.count('')
            for veces in range(contarEspacios):
                separarCuerpo.remove('')

            mensaje = ''
            comando = separarCuerpo[0]
            parametro = separarCuerpo[1] if len(separarCuerpo) > 1 else ''
            if len(separarCuerpo) == 1 and ('AYUDA' in separarCuerpo):
                #print('Pidio ayuda')
                mensaje = self.responder  # self.procesarAyuda()
            elif comando == 'CONSULTAR':
                #print('Consultando su Cita segun cedula {0}'.format(parametro))
                mensaje = self.responder  # 'Su cita esta pautada para el dia dd/mm/aaaa'  # self.procesarConsultar(parametro)
            elif comando == 'CONFIRMAR':
                #print('Confirmando su Cita')
                mensaje = self.responder  # 'Confirmada su cita para el dia dd/mm/aaaa'  # self.procesarConfirmar(parametro)
            elif comando == 'CANCELAR':
                #print('Cancelando la Cita dada a la cedula {0}'.format(parametro))
                mensaje = self.responder  # 'Su cita del dia dd/mm/aaaa fue cancelada con exito'  # self.procesarCancelar(parametro)
            elif comando == 'POSPONER':
                #print('Posponiendo su Cita de la cedula {0} para la fecha xx/xx/xxxxx'.format(parametro))
                mensaje = self.responder  # 'Su cita del dia dd/mm/aaaa fue pospuesta para el dia dd/mm/aaaa'  # self.procesarPosponer(parametro)
            elif comando == 'ELIMINAR':
                #print('Eliminando Numero de Telefono de las Notificaciones')
                mensaje = self.responder  # 'Su numero telefonico fue eliminado de la lista de notificaciones'  # self.procesarEliminar(parametro)
            else:
                #print('Ninguna de las Anteriores')
                mensaje =  self.responder  # procesarNingunaAnteriores()
            '''
            print(numero, cuerpo)
            l = id, numero, mensaje
            listaEnviar.append(l)
        return listaEnviar

    def smsRecibidos(self):
        ''' Metodo que permite buscar los SMS enviados por los
         en bandeja de entrada pacientes y guardarlos en una lista
         para luego ser procesados con el Metodo enviarSocket()
        '''

        listaDevolver = []
        try:
            msg_solicitud = self.droid.smsGetMessages(True)
            #Se recorre la lista de los SMS en el telefono
            #Y se toma solo el ID y el Numero
            for i in msg_solicitud.result:
                id = i['_id']
                telefono = i['address']
                sms = i['body']  # Para versiones Futuras el texto del SMS

                #Si es un numero de telefono normal
                if len(telefono)>=11:
                    listaDevolver.append((id, telefono, sms))
        except:
            self.logger.error('Error al momento de obtener los SMS en la bandeja de entrada del Telefono Android')
        return listaDevolver

    def conectarAndroid(self):
        ''' Metodo que permite recorrer el archivo de configuracion .cfg
        y buscar los telefonos Android conectados via usb para instanciarlos
        para poder obtener los sms en bandeja de entrada'''

        lista = self.buscarServidoresZMQ()

        for server in lista:
            ip_telefono, \
            puerto_telefono, \
            puerto_adb_forward, \
            ip_demonio_zmq, \
            puerto_demonio_zmq, \
            serial_telefono = server

            os.system('adb -s {0} wait-for-device'.format(serial_telefono))
            try:
                self.droid = android.Android((ip_telefono, puerto_adb_forward))
                #print('Conectando el telefono {0}, {1}'.format(ip_telefono, puerto_adb_forward))
                smsListaR = self.smsRecibidos()
                #print(smsListaR)
                smsListaP = self.smsProcesar(smsListaR)
                #print(smsListaP)
                self.enviarSocket(smsListaP)
                time.sleep(5)
            except:
                self.logger.error('No se pudo Conectar con el Telefono Servidor Android')
                self.logger.warning('Redirigiendo el Puerto debido a un error al momento de intentar conectar el telefono Android')
                os.system('adb -s {0} forward tcp:{1} tcp:{2}'.format(serial_telefono, puerto_adb_forward, puerto_telefono))
                time.sleep(10)

    def buscarServidoresZMQ(self):
        ''' Busca en el archivo de configuracion pyloro.cfg todos los
        demonios servidores y devuelve una lista'''

        seccionDemonio = 'DEMONIOS'
        listaServidores= []

        if self.fc.has_section(seccionDemonio):
            for demonios in self.fc.items(seccionDemonio):
                seccion, archivo = demonios
                seccionFinal = seccion.upper()
                if self.fc.has_section(seccionFinal):
                    listaPar = []
                    for var, par in self.fc.items(seccionFinal):
                        listaPar.append(par)

                    ip_telefono, \
                    puerto_telefono, \
                    puerto_adb_forward, \
                    ip_demonio_zmq, \
                    puerto_demonio_zmq, \
                    serial_telefono = listaPar

                    listaServidores.append(listaPar)
                else:
                    self.logger.error('No se encuentra en el archivo de configuracion la Seccion {0}'.format(seccionFinal))
        else:
            self.logger.error('No se cuentra en el archivo de configuracion la Seccion {0}'.format(seccionDemonio))
        return listaServidores

    def main(self):
        ''' '''
        self.logger.info('Proceso iniciado <Contestar al Paciente>')
        self.conectarAndroid()
        self.logger.info('Proceso Finalizado <Contestar al Paciente>')

    def zmqConectar(self):
        ''' Busca en el archivo de configuracion pyloro.cfg todos los
        demonios servidores ZMQ y los conecta'''

        self.socket = ''
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        lista = self.buscarServidoresZMQ()
        for server in lista:
            ip_telefono, \
            puerto_telefono, \
            puerto_adb_forward, \
            ip_demonio_zmq, \
            puerto_demonio_zmq, \
            serial_telefono = server

            servSock = 'tcp://{0}:{1}'.format(ip_demonio_zmq, puerto_demonio_zmq)
            try:
                self.socket.connect(servSock)
                self.logger.info('Conexion Satisfactoria con el servidor {0}'.format(servSock))
            except:
                self.logger.error('Ocurrio un Error al momento de conectar al Socket Server {0}'.format(servSock))

    def run(self):
        ''' Este metodo es el que permite ejecutar el hilo del demonio'''

        '''Ahora sale el mensaje por webService no hace falta copnectar a ZMQ

        #self.zmqConectar() '''

        while True:
            #Es necesario volver a conectar cuando se reincia el telefono
            #Colcoar que si no logra a cpnexpn es xq prbablemente debe estarse reiniiando el telefpmnp, qie espere 2 minutos aprox
            #antes de volver a intentarlo

            self.logger.debug("Debug message")
            self.main()
            time.sleep(60)

#Instancio la Clase
app = smsResponder()
handler = app.configLog()
daemon_runner = runner.DaemonRunner(app)

#Esto garantiza que el identificador de archivo logger no quede cerrada durante daemonization
daemon_runner.daemon_context.files_preserve=[handler.stream]
daemon_runner.do_action()
