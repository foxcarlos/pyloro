import MySQLdb

# Establecemos la conexion con la base de datos
bd = MySQLdb.connect("10.121.6.11","admhc","shc21152115","bdhc" )

# Preparamos el cursor que nos va a ayudar a realizar las operaciones con la base de datos
cursor = bd.cursor()

# Ejecutamos un query SQL usando el metodo execute() que nos proporciona el cursor
x = "select s.firstname, s.lastname,s.mobile from ost_ticket t left join ost_staff s on t.staff_id = s.staff_id  where t.status = 'open'"

cadSQL = '''
select
s.firstname, s.lastname,s.mobile,t.number as numero,
e.name as equipo,
dept_name as Dpto,
t.*
from ost_ticket t
left join ost_staff s on t.staff_id = s.staff_id
left join ost_team e on t.team_id = e.team_id
left join ost_department d on t.dept_id = d.dept_id
where t.status = 'open'
'''

cursor.execute(cadSQL)

# Extraemos una sola fila usando el metodo fetchone()
data = cursor.fetchall()
listaTelefonos = []

for fila in data:
    if fila[2]:
        print(fila[0], fila[1], fila[2], fila[3])
        listaTelefonos.append((fila[2], fila[3]))
print(listaTelefonos)
# Nos desconectamos de la base de datos
bd.close()
