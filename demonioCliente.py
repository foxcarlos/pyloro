#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time
from daemon import runner
import os
import sys
import dbf
import datetime
from rutinas.varias import *
import zmq
import ConfigParser
import urllib
import urllib2

class smsCitas():
    def __init__(self):

        self.nombreArchivoConf = 'pyloro.cfg'
        self.fc = ConfigParser.ConfigParser()

        #Propiedades de la Clase
        self.archivoLog = ''
        self.tiempo = 10

        self.configInicial()
        self.configDemonio()
        self.configPG()

    def configInicial(self):
        '''Metodo que permite extraer todos los parametros
        del archivo de configuracion pyloro.cfg que se
        utilizara en todo el script'''

        #Obtiene Informacion del archivo de Configuracion .cfg
        self.ruta_arch_conf = os.path.dirname(sys.argv[0])
        self.archivo_configuracion = os.path.join(self.ruta_arch_conf, self.nombreArchivoConf)
        self.fc.read(self.archivo_configuracion)

        #Obtiene el nombre del archivo .log para uso del Logging
        seccion = 'RUTAS'
        opcion = 'archivo_log'
        self.archivoLog = self.fc.get(seccion, opcion)

    def configDemonio(self):
        '''Configuiracion del Demonio'''

        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/null'
        self.stderr_path = '/dev/tty'
        self.pidfile_path =  '/tmp/demonioCliente.pid'
        self.pidfile_timeout = 5

    def configLog(self):
        '''Metodo que configura los Logs de error tanto el nombre
        del archivo como su ubicacion asi como tambien los
        metodos y formato de salida'''

        #Extrae de la clase la propiedad que contiene el nombre del archivo log
        nombreArchivoLog = self.archivoLog
        self.logger = logging.getLogger("DemonioCliente")
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter("%(levelname)s--> %(asctime)s - %(name)s:  %(message)s", datefmt='%d/%m/%Y %I:%M:%S %p')
        handler = logging.FileHandler(nombreArchivoLog)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        return handler

    def configPG(self):
        ''' Configuracion para la cadena de conexion de PostGreSQL'''

        seccionPG = 'POSTGRESQL'
        self.host = self.fc.get(seccionPG, 'host')
        self.db = self.fc.get(seccionPG, 'dbname')
        self.user = self.fc.get(seccionPG, 'user')
        self.clave = self.fc.get(seccionPG, 'password')

        self.cadconex = "host='{0}' dbname='{1}' user='{2}' password='{3}'".format\
                (self.host, self.db, self.user, self.clave)

    def formatoMensaje(self, varBuscar, valor1, valor2, valor3, valor4):
        '''parametros recibidos 4:
        Fecha, Especialidad, Telef Contacto, Horario de atencion
        Metodo temporal, que permite obtener los Mensajes del archivo de configuracion .conf
        que se enviaran a los pacientes segun cada necesidad Ejemplo:
        Citas Nuevas, Citas Canceladas y recordatorio de Citas, cada parametro recibido
        se reemplazara con la Variable %FECHA % ESPECIALIDAD %TELEFONO, %HORARIO'''

        linea = ''
        mensaje = ''

        for linea in self.fc.items('MENSAJES'):
            if varBuscar in linea:
                mensaje = linea[1]
                mensaje = mensaje.replace('%FECHA', valor1, 1)
                mensaje = mensaje.replace('%ESPECIALIDAD', valor2, 1)
                mensaje = mensaje.replace('%TELEFONO', valor3, 1)
                mensaje = mensaje.replace('%HORARIO', valor4, 1)
        return mensaje

    def verificarDatos(self, id, numero, fecha_cita, cod_espe):
        '''Parametros recibidos 4: (int id, string numero, datetime fecha_cita, string cod_espe)
        Metodo que permite validar los datos  tomados de las citas de los pacientes
        estos datos deben estar completos de lo contrario no se enviara el SMS al paciente
        parametros devueltos Booleano 1: devuelve Verdadero si consigue algun Error'''

        devolver = False
        ldtFecha = datetime.datetime.strptime(fecha_cita, '%Y-%m-%d')
        ldFecha_Cita = datetime.date(ldtFecha.year, ldtFecha.month, ldtFecha.day)

        if len(numero.strip()) < 11:
            devolver = True
        elif ldFecha_Cita < datetime.date.today():  # Si la fecha de cita es menor al dia actual
            devolver = True
        elif len(cod_espe.strip()) == 0:
            devolver = True
        return devolver

    def buscarEnDbf(self, codEspecialidad):
        '''DBF Consultar la tabla especialidad que se encuentra en una tabla de VFP
        para obtner la descripcion de la especialidad y el telefono a donde se debe llamar'''

        campo = 'file_dbf'
        tabladbf = self.fc.get('RUTAS', campo)
        #print tabladbf
        #tabladbf = archv_dbf
        try:
            tabla_especial = dbf.Table(tabladbf)
            tabla_especial.open()
        except:
            self.logger.error('Error al abrir la tabla DBF')
            sys.exit()

        #Buscar el codigo de la especialidad
        for reg in tabla_especial:
            if reg[1] == codEspecialidad:
                especialidad_desc = reg[2]
                telefonos = reg[5]
                horario = reg[6].strip()
        return especialidad_desc, telefonos, horario

    def consultarCitasNuevas(self):
        '''Metodo que permite buscar en la base de datos las citas nuevas creadas y
        enviarle un sms al usuario con la fecha de la cita y la especialidad'''
        fecha_hoy = datetime.date.today()
        fecha_buscar = fecha_hoy.strftime('%Y-%m-%d')

        pg = ConectarPG(self.cadconex)
        cad_sql = "select id,telefono,fechacons,especial from med_citas_consulta where \
                   fechacons > '%s' and \
                   not efectiva and \
                   not cancelada and \
                   sms_fecha_cita_nueva IS NULL "  % (fecha_buscar)

        registros = pg.ejecutar(cad_sql)
        return registros

    def consultarCitasRecordar(self):
        '''Metodo que permite buscar en la base de datos el dia siguiente y recordarle
        al usuario que tiene cita'''

        dias = 1
        fecha_componer = datetime.date.today() + datetime.timedelta(dias)
        fecha_buscar = fecha_componer.strftime('%Y-%m-%d')

        pg = ConectarPG(self.cadconex)
        cad_sql = "select id,telefono,fechacons,especial from med_citas_consulta \
                   where fechacons = '%s' and \
                   NOT CANCELADA and \
                   NOT EFECTIVA and \
                   sms_fecha_recordar IS NULL" % (fecha_buscar)

        registros = pg.ejecutar(cad_sql)
        return registros

    def consultarCitasCanceladas(self):
        '''Metodo que permite buscar en la base de datos PostGresql las citas que fueron
        canceladas por parte del hospital coromoto y enviarle una notificacion via SMS
        al paciente'''

        fecha_componer = datetime.date.today()
        fecha_buscar = fecha_componer.strftime('%Y-%m-%d')
        pg = ConectarPG(self.cadconex)

        cad_sql = 'select id,telefono,fechacons,especial from med_citas_consulta where \
                   cancelada and \
                   not efectiva and \
                   sms_fecha IS NULL and \
                   fechacons >= $$%s$$' % (fecha_buscar)

        registros = pg.ejecutar(cad_sql)
        return registros

    def procesarCitas(self, registros, tipoDeMensaje):
        '''Parametros recibidos 2:(Tupla Registros, string TipoDeMensaje)
        Ej: Lista, canceladas|nuevas|recordatorio

        Lista:Registros a procesar
        String:Tipo de Mensaje Ej:(canceladas o nuevas o recordatorio)
        Este Metodo permite ordenar mejor los registros y darle un formato legible obteniendo
        el mensaje del archivo de configuracion bot_smscita.conf que se enviara, asi como
        tambien agrupar solo aquellos que cumplen la condicion , es decir aquellos en los
        cuales tienen telefono movil, la fecha de consulta esta correcta etc...'''

        tipoMsg = tipoDeMensaje
        self.registros = registros
        registrosNvos = []
        for paciente in self.registros:
            id = paciente[0]
            telefono = paciente[1]
            fechaCita = '0001-01-01' if paciente[2].year < 1900 else paciente[2].strftime('%d-%m-%Y')
            fechaCita2 = '0001-01-01' if paciente[2].year < 1900 else paciente[2].strftime('%Y-%m-%d')
            codEspecialidad = paciente[3]
            descEspecialidad = ''
            telefonosContactos = ''
            horario = ''

            if not self.verificarDatos(id, telefono, fechaCita2, codEspecialidad):
                descEspecialidad, telefonosContactos, horario = self.buscarEnDbf(codEspecialidad)

                mensaje = self.formatoMensaje(tipoMsg, fechaCita, descEspecialidad.strip(),
                        telefonosContactos.strip(), horario.strip())

                registrosNvos.append((id, telefono, mensaje))
        return registrosNvos

    def enviarWebService(self, numero, mensaje):
        ''' parametros 2, (string:NumeroTelefono, string:MensajeSMS)
        Metodo que permite enviar via web service a pyLoroWeb un SMS
        a cualquier telefono movil
        '''

        print(numero, mensaje)
        url = 'http://10.121.6.12/mensaje'
        data = urllib.urlencode({'numero' : numero, 'mensaje'  : mensaje})
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        respuesta = response.read()
        return respuesta

    def enviarSocket(self, registros, campoTabla):
        '''Parametros 2: (tupla, string):
        (Registros, "String:Nombre del Campo de la Tabla que se actualizara")

        Este Metodo recorre una lista de las personas que se le enviara el
        mensaje SMS de Cita Nueva, Recordatorio o Cita Cancelada para
        luego enviarla via ip por Socket a (el o los) Servidores serverZMQ
        NOTA: Cuando se envia al ServidorZMQ este devuelve mediante self.socket.recv()
        ('1') si todo salio bien o ('0') en caso de fallar'''

        pg = ConectarPG(self.cadconex)

        for fila in registros:
            id, numero, mensaje = fila
            msg = '{0}^{1}'.format(numero, mensaje)

            #Se extrae y muestra en el .log solo una parte del mensaje
            self.logger.info(msg[:100])

            noEnviado = self.enviarWebService(numero, mensaje)
            nombreServidor = 'http://10.121.6.12/mensaje'

            # Antes el nombre del servidor lo devolvia self.socket.recv() ahora no
            #self.logger.info(noEnviado)

            if int(noEnviado):
                #POSTGRES
                cad_update = "update med_citas_consulta set %s =\
                    '%s' where id = %s" % \
                    (campoTabla, datetime.datetime.now().strftime('%Y-%m-%d %I:%M:%S'), id)
                try:
                    pg.ejecutar(cad_update)
                    pg.conn.commit()
                except:
                    logger.error('Ocurrio un error al momento de actualizar la tabla')
                time.sleep(self.tiempo)
                # FIN POSTGRES
            else:
                self.logger.warn('Houston Tenemos un Problema con el ID:{0},\
                    no se pudo enviar el SMS dede el Servidor {1}'.format(id, nombreServidor))

        pg.cur.close()
        pg.conn.close()

    def hoy(self):
        '''Metodo que permite procesar y enviar sms a los pacientes que
        han tomado citas nuevas'''

        self.logger.info('Proceso Iniciado <Citas Nuevas>')
        r = self.consultarCitasNuevas()
        final = self.procesarCitas(r, 'nuevas')
        self.enviarSocket(final, 'sms_fecha_cita_nueva')
        self.logger.info('Proceso Terminado <Citas Nuevas>')
        return True

    def recordar(self):
        '''Metodo que permite procesar y enviar sms de recordatorio
        de cita un dia antes de su consulta '''

        self.logger.info('Proceso iniciado <Citas Recordar>')
        r2 = self.consultarCitasRecordar()
        final2 = self.procesarCitas(r2, 'recordatorio')
        print(final2)

        self.enviarSocket(final2, 'sms_fecha_recordar')
        self.logger.info('Proceso terminado <Citas Recordar>')
        return True

    def cancelar(self):
        '''Metodo que permite procesar y enviar sms a los pacientes
        cuando su cita es cancelada '''

        self.logger.info('Proceso iniciado <Citas Canceladas>')
        r3 = self.consultarCitasCanceladas()
        final3 = self.procesarCitas(r3, 'canceladas')
        self.enviarSocket(final3, 'sms_fecha')
        self.logger.info('Proceso Terminado <Citas Canceladas>')
        return True

    def run(self):
        ''' Este metodo es el que permite ejecutar el hilo del demonio'''

        #En Desuso
        #self.zmqConectar()
        while True:
            self.logger.debug("Debug message")
            #self.logger.info("Info message")
            #self.logger.warn("Warning message")
            #self.logger.error("Error message")

            self.cancelar()
            time.sleep(60)

            self.hoy()
            time.sleep(60)

            self.recordar()
            time.sleep(60)

#Instancio la Clase
app = smsCitas()
handler = app.configLog()
daemon_runner = runner.DaemonRunner(app)

#Esto garantiza que el identificador de archivo logger no quede cerrada durante daemonization
daemon_runner.daemon_context.files_preserve=[handler.stream]
daemon_runner.do_action()
