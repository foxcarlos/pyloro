#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
**************************************************************************************************
 Realizado por:Carlos Alberto Garcia Diaz
 email:foxcarlos@gmail.com
 twitter: @foxcarlos
**************************************************************************************************
 Script que solicita el puerto del telefono que actua como servidor de SMS y direcciona
 el ADB hacia ese puerto, esto es debido a que cuando el telefono se apaga hay que
 activarlo nuevamente y el puerto cambia
'''

import os


def iniciar():
    var_puerto = raw_input('Ingrese el Puerto del Telefono:')
    comando = "/home/cgarcia/Aplicaciones/android-sdk-linux/platform-tools/adb forward tcp:9999 tcp:%s" % (var_puerto)
    #comando = "/home/cgarcia/Aplicaciones/android-sdk-linux/platform-tools/"
    print(comando)
    os.system(comando)


if __name__ == '__main__':
    iniciar()
