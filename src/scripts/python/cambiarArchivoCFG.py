#! /usr/bin/env python
# -*- coding: utf-8 -*-

from ConfigParser import SafeConfigParser

class cambiarConfiguracion():
    def __init__(self, archivo):
        ''' '''
        self.cfgArchivo = archivo
        self.parser = SafeConfigParser()
        self.parser.read(self.cfgArchivo)
        
        seccion = raw_input('Indique la Seccion donde se encuentra la Opcion que desea modificar:')
        seccion = seccion.upper()  # Las secciones deben estar siempre en mayusculas
        opcionModificar = raw_input('Indique la Opcion que desea modificar:')
        #valorActualOpcion = self.parser.get(seccion, opcionModificar)
        #print 'Valor Original :{0}'.format(valorActualOpcion)
         
        valorNuevoOpcion = raw_input('Ingrese el nuevo valor para {0}:'.format(opcionModificar))
        self.parser.set(seccion, opcionModificar, valorNuevoOpcion)
        print('Nuevo Valor para {0} ahora es: {1}'.format(opcionModificar, self.parser.get(seccion, opcionModificar)))
        
        abrirArchivo = open(self.cfgArchivo, 'w')
        self.parser.write(abrirArchivo)
        abrirArchivo.close()

if __name__ == '__main__':
    lcArchivo = '/home/cgarcia/desarrollo/python/pyloro/pyloro.cfg'
    app = cambiarConfiguracion(lcArchivo)
