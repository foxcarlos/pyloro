#!/usr/bin/env python

#importar zeromq
import zmq
import time
import os
from rutinas.varias import *
import ConfigParser

'''
#Se crea una instancia del contexto
context = zmq.Context()
#Se crea el socket con parametro peticion (REQ)
socket = context.socket(zmq.REQ)
#Se crean 2 sockets con la IP y los 2 puertos donde escucha
#los 2 servidores.
socket.connect("tcp://10.121.3.32:5000")
socket.connect("tcp://10.121.3.31:6000")
'''

tiempo = 10

class test():
    def __init__(self):
        ruta_arch_conf = os.path.dirname(sys.argv[0])
        archivo_configuracion = os.path.join(ruta_arch_conf, '../../../pyloro.cfg')
        self.fc = ConfigParser.ConfigParser()
        self.fc.read(archivo_configuracion)
        self.zmqConectar()

    def zmqConectar(self):
        ''' Busca en el archivo de configuracion pyloro.cfg todos los 
        demonios servidores ZMQ y los conecta'''
        
        self.socket = ''
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        seccionDemonio = 'DEMONIOS'
        
        if self.fc.has_section(seccionDemonio):
            for demonios in self.fc.items(seccionDemonio):
                seccion, archivo = demonios
                seccionFinal = seccion.upper()
                if self.fc.has_section(seccionFinal):
                    listaPar = []
                    for var, par in self.fc.items(seccionFinal):
                        listaPar.append(par)
                    print(listaPar)               
                    ip_telefono, \
                    puerto_telefono, \
                    puerto_adb_forward, \
                    ip_demonio_zmq, \
                    puerto_demonio_zmq, \
                    serial_telefono = listaPar
                    servSock = 'tcp://{0}:{1}'.format(ip_demonio_zmq, puerto_demonio_zmq)
                    try:
                        self.socket.connect(servSock)
                        print('Conexion Satisfactoria con el servidor {0}'.format(servSock))
                    except:
                        print('Ocurrio un Error al momento de conectar al Socket Server {0}'.format(servSock))

    def main(self):
        rango = raw_input('Ingrese el rango a repetir el sms o presione enter si solo desea enviar 1 SMS:')
        telefonos = raw_input('Ingrese el o los  Numeros Telefonico separados por coma o la ruta y el archivo que contiene los numeros:')
        mensaje = raw_input('Ingrese el Mensaje a enviar:')
        
        #Verifica si lo ingresado es una ruta y archivo
        if os.path.isfile(telefonos):
            f = telefonos  # '/home/cgarcia/TelefonosDifusionMediosCamaradas.csv'
            ar = open(f)
            num = [t for t in ar.readlines()]
            num2 = (num[0].split(','))
            telefonos = num2
            #print(telefonos)
        else:
            telefonos = telefonos.split(',')

        rango = int(rango) if rango  else 1
        telefonos = telefonos if telefonos  else '04263002966'
        mensaje = mensaje if mensaje  else 'Test de Prueba'

        for veces in range(rango):
            for telefono in telefonos:
                msg = '{0}^{1}'.format(telefono, mensaje)
                try:
                    self.socket.send(msg)
                except zmq.ZMQError:
                    e = sys.exc_info()[1]
                    print(e)
                    break
                #Se muestra en pantalla el mensaje enviado
                print "Enviando Mensaje nro:{0}".format(veces)
                #Se recibe el mensaje de vuelta
                msg_in = self.socket.recv()
                #raise Exception('Unfortunately this failed')
            
                noEnviado, nombreServidor = msg_in.split(',')
                if int(noEnviado):
                    print 'Todo Salio Bien'
                else:
                    print 'Houston tenemos un Problema'
                    time.sleep(10)

if __name__ == '__main__':
    t = test()
    t.main()
