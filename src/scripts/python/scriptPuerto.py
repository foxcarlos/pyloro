#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

'''
- Es el mismo comando tanto para conexion via USB o WIFI

- Es  probable que en algunos casos para ejecutar el comando adb
  sea necesario hacerlo como usuario root en linux.

- Se debe realizar el siguiente comando para matar el servidor:
./android-sdk-linux/platform-tools/adb kill-server

- Luego se levanta el servidor adb:
./android-sdk-linux/platform-tools/adb forward tcp:9999 tcp:33641

Si el servidor sl4a en el telefono se levanta publico es decir que 
la comunicacion es via wifi entonces se toma el puerto de la variable
de ambiente, de lo contrario si el servidor de privado es decir si el 
telefono se conecta via usb entonces el puerto sera "9999"
'''

tipo_conexion = raw_input('Ingrese el Tipo de Conexion: escriba  USB o WIFI:')
ip = raw_input('Ingrese la IP:')
puertoTelefono = raw_input('Ingrese el Puerto que indica el Telefono:')

if tipo_conexion:
    if tipo_conexion.upper() == 'USB':
        ip = '127.0.0.1'
        entornoPuerto = '9999'
        comando = "/home/cgarcia/android-sdk-linux/platform-tools/adb forward tcp:9999 tcp:%s" % (puertoTelefono)
    elif tipo_conexion.upper() == 'WIFI':
        entornoPuerto = puertoTelefono
        comando  = "/home/cgarcia/android-sdk-linux/platform-tools/adb start-server"
    else:
        print 'Tipo de Conexion Incorrecta verifique que ingreso o WIFI o USB'
        sys.exit

os.system(comando)
os.environ["AP_HOST"] = ip
os.environ["AP_PORT"] = entornoPuerto

print  os.getenv("AP_HOST"), os.getenv("AP_PORT")
'''
s = Sms()
ippuerto = os.getenv("AP_HOST"), os.getenv("AP_PORT")
s.iniciar(ippuerto)
'''
