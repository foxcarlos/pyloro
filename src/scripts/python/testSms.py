#!/usr/bin/python
# -*- coding: utf-8 -*-

from rutinas import varias as sms
from time import sleep
import android
import os

class test():
    def __init__(self, numero, mensaje, repetir=1):
        '''Parametros 3, (String: Numero de Telefono, String: Mensaje, Integer: Veces a Repetir el SMS )
        Este Metodo Recibe 3 parametros con el cual se configuran los SMS a enviar'''

        self.numero = numero
        self.mensaje = mensaje
        self.repetir = repetir
        self.conectar()

    def conectar(self):
        '''Metodo para conectar con el Telefono Android conectado via Cable USB, utiliza la
        biblioteca Android'''

        #self.s = sms.Sms()
        #self.s.iniciar(('127.0.0.1', '9999'))

        #print('Reiniciando el telefono...')
        #os.system('adb reboot')

        #print('Esperando mientras el telefono se reinicia')
        #sleep(120)

        #print('Iniciando SL4A en el Telefono')
        #cmd = '''adb shell am start -a com.googlecode.android_scripting.action.LAUNCH_SERVER -n com.googlecode.android_scripting/.activity.ScriptingLayerServiceLauncher --ei com.googlecode.android_scripting.extra.USE_SERVICE_PORT 9796'''
        #os.system(cmd)
        #sleep(15)

        #print('Redirigiendo el Puerto...')
        #os.system('adb forward tcp:9999 tcp:9796')
        #sleep(10)

        self.s = android.Android(('127.0.0.1', '9999'))

    def boom(self):
        ''' Metodo que ejecuta los SMS segun los parametros enviados en el Init'''

        c = 0
        for i in range(self.repetir):
            #Esta condicion obliga a reconectar de nuevo la clase
            #luego de enviar mas de 98 sms, esto solo se hizo
            #para hacer unas pruebas, nunca se a probado en produccion

            if c >= 98:
                c = 0
                self.conectar()
                reload(android)
                sleep(180)

            msg = '{0} Nro:{1}'.format(self.mensaje, i)
            print(msg)
            self.s.smsSend(self.numero, msg)
            sleep(5)
            c = c + 1

if __name__ == '__main__':
    t = test('04263002966', 'prueba', 1000)
    t.boom()
    #t = test('04246514597', 'Hola Perrina', 10)
    #t.boom()

