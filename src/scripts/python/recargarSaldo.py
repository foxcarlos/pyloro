#!/usr/bin/env python

import time
from daemon import runner
import logging
import os
import sys
import zmq
from rutinas import varias
from ConfigParser import SafeConfigParser

class llamarNumero():
    def __init__(self):
        '''Metodo Init donde se inicializan
        todos los procesos para dar comienzo
        al Demonio'''

        #Para saber como se llama este archivo .py que se esta ejecutando
        archivo = sys.argv[0]  # Obtengo el nombre de este  archivo
        archivoSinRuta = os.path.basename(archivo)  # Elimino la Ruta en caso de tenerla
        self.archivoActual = archivoSinRuta

        self.nombreArchivoConf = 'pyloro.cfg'
        self.fc = SafeConfigParser()

        #Propiedades de la Clase
        self.archivoLog = ''

        #Ejecutar los Procesos Inciales
        self.configInicial()
        self.verificaDemonio()
        self.telefonoServidor()

    def configInicial(self):
        '''Metodo que permite extraer todos los parametros
        del archivo de configuracion pyloro.conf que se
        utilizara en todo el script'''

        #Obtiene Informacion del archivo de Configuracion .cfg
        self.ruta_arch_conf = os.path.dirname(sys.argv[0])
        self.archivo_configuracion = os.path.join(self.ruta_arch_conf, '../../../pyloro.cfg')  # os.path.join(self.ruta_arch_conf, self.nombreArchivoConf)
        self.fc.read(self.archivo_configuracion)

        #Obtiene el nombre del archivo .log para uso del Logging
        # (RUTAS Y archivo.log son los campos del archivo .cfg)
        seccion = 'RUTAS'
        opcion = 'archivo_log'
        self.archivoLog = self.fc.get(seccion, opcion)

    def configLog(self):
        '''Metodo que configura los Logs de error tanto el nombre
        del archivo como su ubicacion asi como tambien los
        metodos y formato de salida'''

        #Extrae de la clase la propiedad que contiene el nombre del archivo log
        nombreArchivoLog = self.archivoLog
        self.logger = logging.getLogger("{0}".format(self.archivoActual))
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter("%(levelname)s--> %(asctime)s - %(name)s:  %(message)s", datefmt='%d/%m/%Y %I:%M:%S %p')
        handler = logging.FileHandler(nombreArchivoLog)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        return handler

    def verificaDemonio(self):
        '''Obtengo del archivo de configuracion el  Nombre del Demonio
        con su IP y Puerto, Cada demonio que se ejecute debe estar en el
        archivo de configuracion con el nombre del archivo demonio y
        su respetiva direccion IP y Puerto al cual escucha,
        en la seccion Ej:
        [DEMONIOS]
        demonio1 = demonioserver1.py 
        demonio2 = demonioserver2.py 
 
        [DEMONIO1]
        ip_telefono = 127.0.0.1
        puerto_telefono = 9796
        puerto_adb_forward = 1111
        ip_demonio_zmq = 10.121.3.48
        puerto_demonio_zmq = 6000'''
        
        seccionDemonio = 'DEMONIOS'
        #Verfico si existe la seccion DEMONIO en el .cfg
        if self.fc.has_section(seccionDemonio):
            #Si Existe listo el contenido de la Seccion
            #Y busco si esta este archivo .py configurado alli
            for demonios in self.fc.items(seccionDemonio):
                #Reccoro la lista y verifico si esta alli
                if self.archivoActual.lower() in demonios:
                    #Si lo consigue guardo el nombre de la seccion y el nombre del archivo
                    seccion, archivo = demonios
                    seccionFinal = seccion.upper()
                    #Ahora busco si existe dicha seccion
                    if self.fc.has_section(seccionFinal):
                        listaPar = []
                        for var, par in self.fc.items(seccionFinal):
                            listaPar.append(par)
                        
                        self.ip_telefono, \
                        self.puerto_telefono, \
                        self.puerto_adb_forward, \
                        self.ip_demonio_zmq, \
                        self.puerto_demonio_zmq, \
                        self.serial_telefono = listaPar
                    else:
                        msg = 'En el archivo de configuracion {0}\
                                no se encuentra configurada la seccion {1}\
                                en la que se hizo referencia en la seccion {2}'.format(self.nombreArchivo, seccionFinal, seccionDemonio)
                else:
                    msg = ' En el archivo de configuracion {0}\
                            Dentro de la Seccion [1]\
                            no se encuentra configurado este archivo {2}\
                            en ningun items u Opcion'.format(self.nombreArchivoConf, seccionDemonio, self.archivoActual)
        else:
            msg = 'No se encuentra la Seccion {0}\
                    en el archivo de configuracion {1}'.format(seccionDemonio, self.nombreArchivoConf)
            self.logger.error(msg.strip())
            sys.exit()

    def telefonoServidor(self):
        '''Obtengo de la seccion IP_TELEFONOS y Opcion telefono
        y separo en una una tupla la ip y el puerto del telefono
        que enviara los SMS'''

        self.telefonoIpPuerto = self.ip_telefono, self.puerto_telefono
    
    def conectarAndroid(self):
        #import android
        #self.droid = android.Android((self.ip_telefono, self.puerto_adb_forward))
        try:
            self.droid = varias.Sms()
            self.droid.iniciar((self.ip_telefono, self.puerto_adb_forward))
            msg = 'Conexion con el teleofno iniciado con Exito en la\
                    Ip {0} y en el puerto {1}'.format(self.ip_telefono, self.puerto_adb_forward)
            self.logger.info(msg)
        except:
            msg = 'Error Intentando conectar el Telefono Android IP:{0} Puerto:{1}'.format(self.ip_telefono, self.puerto_adb_forward)
            self.logger.error(msg)
            
            self.logger.warning('Redirigiendo el Puerto debido a un error al momento de intentar conectar el telefono Android')
            os.system('adb -s {0} forward tcp:{1} tcp:{2}'.format(self.serial_telefono, self.puerto_adb_forward, self.puerto_telefono))
            time.sleep(10)

    def llamar(self):
        ''' Metodo que permite hacer el Marcado telefonico'''

        self.conectarAndroid()
        
        codigo = raw_input('Ingrese el codigo de la tarjeta Telefonica para recargar')
        marcar = '*21'+codigo
        self.droid.phoneCallNumber(marcar)
        self.logger.info('Felicidades..!, se realizo la llamada al numero:{0} con Exito'.format(marcar))

if __name__ == '__main__':
    app = llamarNumero()
    app.llamar()


