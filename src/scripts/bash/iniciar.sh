#!/bin/bash

export PYTHONPATH=$PYTHONPATH:/home/cgarcia/desarrollo/python/
export PATH=$PATH:/home/cgarcia/Aplicaciones/android-sdk-linux/platform-tools/
export  http_proxy=http://10.121.6.12:8080 

#--------------------------------------------------------------------------------------------------------------------------------------------------
#CUANDO EL PC ES DEMONIO SERVIDOR
#--------------------------------------------------------------------------------------------------------------------------------------------------

#Levantar el SL4A en el telefono, es necesario que este conectado el telefono via cable USB

echo "Reiniciando el telefono"
adb reboot

echo "Esperando mientras el telefono se reincia"
sleep 2m 

echo "Arrancando SL4A en el Telefono"
adb shell am start -a com.googlecode.android_scripting.action.LAUNCH_SERVER -n com.googlecode.android_scripting/.activity.ScriptingLayerServiceLauncher --ei com.googlecode.android_scripting.extra.USE_SERVICE_PORT 9796

sleep 15s

echo "Redirigiendo el Puerto adb del Telefono"
adb forward tcp:9999 tcp:9796

#Para el PC que ejecute el demonioServidor.py y tengan los telefonos conectados por USB
sh /etc/init.d/demonioServidor.sh stop
sh /etc/init.d/demonioServidor.sh start

#--------------------------------------------------------------------------------------------------------------------------------------------------
#CUANDO EL PC ES DEMONIO CLIENTE
#--------------------------------------------------------------------------------------------------------------------------------------------------

#Para el PC que ejecute el demonioCliente.py
sh /etc/init.d/demonioCliente.sh stop
sh /etc/init.d/demonioCliente.sh start
