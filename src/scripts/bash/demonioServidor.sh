case "$1" in
  start)
    echo "Iniciando el ServerZMQ"
    # Start the daemon 
    python /home/cgarcia/desarrollo/python/pyloro/demonioServer1.py start
    ;;
  stop)
    echo "Deteniendo el ServerZMQ"
    # Stop the daemon
    python /home/cgarcia/desarrollo/python/pyloro/demonioServer1.py stop
    ;;
  restart)
    echo "Reiniciando el ServerZMQ"
    python /home/cgarcia/desarrollo/python/pyloro/demonioServer1.py restart
    ;;
  *)
    # Refuse to do other stuff
    echo "Ejecuta : /etc/init.d/demonioServer.sh {start|stop|restart}"
    exit 1
    ;;
esac

exit 0

