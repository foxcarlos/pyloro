case "$1" in
  start)
    echo "Iniciando el ServerZMQ"
    # Start the daemon 
    python /home/cgarcia/desarrollo/python/pyloro/demonioCliente.py start
    ;;
  stop)
    echo "Deteniendo el ServerZMQ"
    # Stop the daemon
    python /home/cgarcia/desarrollo/python/pyloro/demonioCliente.py stop
    ;;
  restart)
    echo "Reiniciando el ServerZMQ"
    python /home/cgarcia/desarrollo/python/pyloro/demonioCliente.py restart
    ;;
  *)
    # Refuse to do other stuff
    echo "Ejecuta : /etc/init.d/demonioCliente.sh {start|stop|restart}"
    exit 1
    ;;
esac

exit 0

