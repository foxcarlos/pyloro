#!/bin/bash

echo "Reiniciando el telefono"
adb reboot

echo "Esperando mientras el telefono se reincia"
sleep 2m 

adb wait-for-device

echo "Arrancando SL4A en el Telefono"
adb shell am start -a com.googlecode.android_scripting.action.LAUNCH_SERVER -n com.googlecode.android_scripting/.activity.ScriptingLayerServiceLauncher --ei com.googlecode.android_scripting.extra.USE_SERVICE_PORT 9796

sleep 15s

echo "Redirigiendo el Puerto adb del Telefono"
adb forward tcp:9999 tcp:9796

echo "Proceso Terminado"
