import urllib
import urllib2
import sys

def enviarWebService(numero, mensaje):
    ''' parametros 2, (string:NumeroTelefono, string:MensajeSMS)
    Metodo que permite enviar via web service a pyLoroWeb un SMS
    a cualquier telefono movil
    '''

    print(numero, mensaje)
    url = 'http://10.121.6.12/mensaje'
    data = urllib.urlencode({'numero' : numero, 'mensaje'  : mensaje})
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    respuesta = response.read()
    return respuesta

def main(numero, mensaje):
    enviarWebService(numero, mensaje)

if __name__ == '__main__':
    try:
        numero = sys.argv[1]
        mensaje = sys.arv[2]
        print(numero)
    except:
        numero = ''
        mensaje = ''

    main('04165602966', 'mensaje')
