select 
s.firstname, s.lastname,s.mobile,
e.name as equipo, 
dept_name as Dpto,
t.* 
from ost_ticket t 
left join ost_staff s on t.staff_id = s.staff_id 
left join ost_team e on t.team_id = e.team_id 
left join ost_department d on t.dept_id = d.dept_id 
where t.status = 'open'

