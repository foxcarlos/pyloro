import urllib
import urllib2
import time

def enviarWebService(numero, mensaje):
    ''' parametros 2, (string:NumeroTelefono, string:MensajeSMS)
    Metodo que permite enviar via web service a pyLoroWeb un SMS
    a cualquier telefono movil
    '''
    
    #print(numero, mensaje)
    url = 'http://10.121.6.12/mensaje'
    data = urllib.urlencode({'numero' : numero, 'mensaje'  : mensaje})
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    print(response.read())
    response.close
    return response

def iniciar():
    registros = [('04165602966', 'Hola1'),('04165602966', 'Hola2'), ('04165602966', 'Hola3'), ('04165602966', 'Hola4')]
    for fila in registros:
        numero, mensaje = fila
        #print(numero, mensaje)
        noEnviado = enviarWebService(numero, mensaje)
#         time.sleep(20)
#         if int(noEnviado):
#             print('Se envio correctamente el mensaje:{0}'.format(noEnviado))            
#         else:
#             print('No se logro enviar el mensaje:{0}'.format(noEnviado))
        
if __name__ == '__main__':
    iniciar()
    

