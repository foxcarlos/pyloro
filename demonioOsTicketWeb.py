import MySQLdb
import urllib
import urllib2

#***********Pagina temporal para envio de mensajes de texto***********#
from bottle import route, run, Bottle, template, request, static_file, error
import sys

app = Bottle()


class mySql:
    def __init__(self):
        pass

    def conectar(self):
        # Establecemos la conexion con la base de datos

        #self.bd = MySQLdb.connect("10.121.6.11","admhc","shc21152115","bdhc" )
        self.bd = MySQLdb.connect("10.121.6.11","root","shc21152115","bdost", port=3306)

        # Preparamos el cursor que nos va a ayudar a realizar las operaciones con la base de datos
        self.cursor = self.bd.cursor()

    def consultar(self, strSql):
        ''' Cadena SQL a ejecutar'''
        # Ejecutamos un query SQL usando el metodo execute() que nos proporciona el cursor


        cadSQL = strSql
        self.cursor.execute(cadSQL)

        # Extraemos una sola fila usando el metodo fetchone()
        data = self.cursor.fetchall()
        self.bd.close()

        return data


def ordenesAbiertas():
    '''Metodo que permite consultar la Ordenes de
    Servicios Abiertas por los usuarios'''

    cadSQL = '''
    select
    s.firstname, s.lastname,s.mobile,t.number as Ods,
    e.name as equipo,
    dept_name as Dpto,
    u.name as usuario,
    t.*
    from ost_ticket t
    left join ost_staff s on t.staff_id = s.staff_id
    left join ost_team e on t.team_id = e.team_id
    left join ost_department d on t.dept_id = d.dept_id
    left join ost_user u on t.user_id = u.id
        where t.status_id = 1
   '''
   #status_id = 1 open
#dd
#'''            t.ticket_id not in (
#                select ticket_id from ost_ticket_thread
#                    where (DATEDIFF(NOW(),date(created))>=0 and DATEDIFF(NOW(),date(created))<=1)
#                    order by created desc))'''

    cnxMySql = mySql()
    cnxMySql.conectar()
    data = cnxMySql.consultar(cadSQL)

    listaTelefonos = []

    for fila in data:
        if fila[2]:
            #print(fila)
            #print(fila[0], fila[1], fila[2], fila[3])
            listaTelefonos.append((fila[2], fila[3], fila[6]))

    for f in listaTelefonos:
        numTelf, numOds, nomUsuario = f
        msjEnviar = 'Ud tiene una ODS abierta bajo el #{0} de {1}'.format(numOds, nomUsuario)
        enviarWebService(numTelf, msjEnviar)
        enviarWebService('04166661992', msjEnviar)


def ordenesAbiertasSinTomar():
    '''Hay que buscar la tabla de hilos y verificar si tiene Hilos
    y verificar en caso de tener algun Hilo cuando fue
    la ultima vez que el usuario comento la ODS segun ese hilo '''

    cadSQL = '''
    select
    s.firstname, s.lastname,s.mobile,t.number as Ods,
    e.name as equipo,
    dept_name as Dpto,
    u.name as usuario,
    t.*
    from ost_ticket t
    left join ost_staff s on t.staff_id = s.staff_id
    left join ost_team e on t.team_id = e.team_id
    left join ost_department d on t.dept_id = d.dept_id
    left join ost_user u on t.user_id = u.id
    left join ost_ticket_thread  hilo on t.ticket_id = hilo.ticket_id
    where t.status = 'open' and (t.staff_id = hilo.staff_id and DATE(hilo.created)= CURDATE())
    select ticket_id, created,DATEDIFF(NOW(),date(created)) as tiempo from ost_ticket_thread where ticket_id = '2261' and DATEDIFF(NOW(),date(created))<=2

    '''


def enviarWebService(numero, mensaje):
    ''' parametros 2, (string:NumeroTelefono, string:MensajeSMS)

    Metodo que permite enviar via web service a pyLoroWeb un SMS
    a cualquier telefono movil
    '''

    print(numero, mensaje)
    url = 'http://10.121.6.12/mensaje'
    data = urllib.urlencode({'numero' : numero, 'mensaje'  : mensaje})
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    respuesta = response.read()
    print(respuesta)
    print(type(respuesta))
    #print(response.read())


#if __name__ == '__main__':
#    ordenesAbiertas()



@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename, root='static/')

@app.route('/osticket/sms', method='GET')
def mostrar_opciones():
    # Funcion que muestra la pagina con botones de accion
    output = template('opciones.html')
    return output

@app.route('/OSTicket/SMS/enviar', method='POST')
def enviar_mensajes():
    # Funcion que envia los mensajes
    ordenesAbiertas()
    output = """<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <title></title>
                </head>
                <body>
                    <h1>Mensajes enviados</h1>
                    <a href='/OSTicket/SMS'>Inicio</a>
                </body>
                </html>"""
    return output




run(app, host='0.0.0.0', port=8091, reloader=True)

