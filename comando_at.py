#!/usr/bin/env python
'''
***********************************************************************
Realizado por:Carlos Alberto Garcia Diaz
email:foxcarlos@gmail.com
Google Plus:foxcarlos@gmail.com
twitter: @foxcarlos
***********************************************************************
        Script para el envio de SMS via comandos AT
        En mi caso el puerto es /dev/ttyACM0 el cual
        luego de conectar el telefono con el cable
        al puerto usb lo busque ejecutando desde consola
        el comando dmesg desde linux que es el sistema
        operativo en el cual trabajo, el comando me muestra
        algo asi:
        [52096.163146] usb 4-2: Product: Nokia 2690
        [52096.163148] usb 4-2: Manufacturer: Nokia
        [52096.221302] cdc_acm 4-2:1.1: ttyACM0: USB ACM device

        NOTA: Antes de trabajarlo con python primero hice
        pruebas con minicom que es lo que para winows seria
        hyper terminal si no sabes usar minicom aqui les
        dejo un link:
        http://www.lodemenos.net/Hypertermial-en-Linux-se-llama.html
'''
# usado para dividir el mensaje que se desea enviar via sms en varias partes
from __future__ import division
import math
#---------------------------------------------------------------------------

import serial
import time


class SmsAt():
    '''
    Clase que permite enviar un SMS via comando At
    Ej:
    #Instanciar la Clase
    sms = SmsAt('+584263000000', '/dev/ttyACM0')
    sms.enviar('Prueba de SMS')
    '''

    def __init__(self, numtlf, puerto):
        '''
        parametros recibidos 2: string Numero de telefono, string Puerto
        Ej: SmsAt('+584263000000', '/dev/ttyACM0')
        '''

        self.numero = numtlf
        self.puerto = puerto  # Ej:'/dev/ttyACM0'
        self.tiempo = 1

    def enviar(self, cadena_sms):
        sms_lista = self.verificar_lontitud(cadena_sms)
        try:
            for mensaje in sms_lista:
                self.enviar_at(mensaje)
                valor_devuelto = True
                time.sleep(3)
        except:
            valor_devuelto = False
        return valor_devuelto

    def enviar_at(self, mensaje):
        '''
        Parametros 1: string Mensaje de texto a Enviar
        Ej: enviar('Hola Mundo')
        '''

        telefono = serial.Serial(self.puerto, 460800, timeout=5)
        try:
            time.sleep(self.tiempo)

            telefono.write('ATZ\r')
            time.sleep(self.tiempo)

            telefono.write('AT+CMGF=1\r')
            time.sleep(self.tiempo)

            telefono.write('AT+CMGS="%s"\r' % (self.numero))
            time.sleep(self.tiempo)

            msj = '%s \r' % (mensaje)
            telefono.write(msj + chr(26))
            #chr(26) equivale a CTRL+Z q es la forma de terminar en comandos AT
            time.sleep(self.tiempo)

            telefono.close()
        finally:
            telefono.close()

    def verificar_lontitud(self, msg):
        '''(Parametros recibidos: 1, string)
         Descripcion : Metodo que permite dividir un mensaje en
         varias partes debido a que las companias telefonicas en Venezuela solo permite
         enviar SMS de 160 caracteres, al final de cada mensaje enviado se indicara la posicion
         actual y la cantitad Ej: si el mensaje es largo y se tiene que dividir en 3 entonces
         al final de cada sms se colocara 1/3, que significa 1 sms de 3 en total '''

        limite = 154.0  # Se coloca 154 porque faltan 4 caracteres que indica sms 1 de 2 ej 1/2
        inicio = 0
        print msg
        longi = len(msg.strip())
        rango = int(math.ceil(float(longi) / limite))
        lista_devolver = []

        for veces in range(rango):
            cad_final = '%s (%s/%s)' % (msg[inicio:int(limite)], veces + 1, int(rango))
            lista_devolver.append(cad_final.strip())
            inicio = inicio + 154
            limite = limite + 154
        return lista_devolver

if __name__ == '__main__':
    numtlf = '+584263002966'  # +codigo_pais_seguido_del numero
    
    numtlf2 = '+584263002966'  # +codigo_pais_seguido_del numero
    
    mensaje = 'Hola Mundo'
    
    #puerto = '/dev/ttyACM0'
    puerto = '/dev/ttyUSB0'
    puerto2 = '/dev/ttyUSB1'

    #Instancio la Clase
    sms = SmsAt(numtlf, puerto)
    sms.enviar(mensaje)

    sms = SmsAt(numtlf2, puerto2)
    x= sms.enviar(mensaje)
    print x
