__author__ = 'cgarcia'

import zmq
import ConfigParser
import bottle
import sys
import os
from bottle.ext.websocket import GeventWebSocketServer
from bottle import template


class enviarZMQ():

    def __init__(self):
        ruta_arch_conf = os.getcwdu()  # os.path.dirname(sys.argv[0])

        # NOTA: Es necesario que exista fuera de la ruta actual un directorio llamado pyloro
        fi = '/opt/webservice/configuracion/pyloro.cfg'
        archivo_configuracion = os.path.join(ruta_arch_conf, fi)
        self.fc = ConfigParser.ConfigParser()
        self.fc.read(archivo_configuracion)
        # self.zmqConectar()

    def zmqCConectar(self):
        ''' Busca en el archivo de configuracion pyloro.cfg todos los
        demonios servidores ZMQ y los conecta'''

        self.socket = ''
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        seccionDemonio = 'DEMONIOS'
        print('Me Llamo')
        if self.fc.has_section(seccionDemonio):
            print(self.fc.items(seccionDemonio))
            veces = 1
            for demonios in self.fc.items(seccionDemonio):
                # print('demonios', demonios)
                print(veces)
                veces = veces + 1
                seccion, archivo = demonios
                seccionFinal = seccion.upper()
                if self.fc.has_section(seccionFinal):
                    # listaPar = []
                    # print('Seccion Final', seccionFinal)
                    # for var, par in self.fc.items(seccionFinal):
                    #    listaPar.append(par)
                    # print('ListaPar', listaPar)

                    listaPar = [parametros[1] for parametros in self.fc.items(seccionFinal)]
                    print(listaPar)

                    ip_telefono, \
                        puerto_telefono, \
                        puerto_adb_forward, \
                        ip_demonio_zmq, \
                        puerto_demonio_zmq, \
                        serial_telefono = listaPar
                    servSock = 'tcp://{0}:{1}'.format(ip_demonio_zmq, puerto_demonio_zmq)
                    try:
                        self.socket.connect(servSock)
                        # print('Conexion Satisfactoria con el servidor {0}'.format(servSock))
                    except zmq.ZMQError, e:
                        exceptionType, exceptionValue, exceptionTraceback = sys.api_versionexc_info()
                        # print('Ocurrio un Error al momento de conectar al Socket Server {0}'.format(servSock))
                        print('Ocurrio un Error al momento de conectar al Socket Server {0}'.format(e))

    def enviar(self, telefono, mensaje):
        msg = '{0}^{1}'.format(telefono, mensaje)
        devuelve = True

        try:
            self.socket.send(msg)
        except zmq.ZMQError:
            e = sys.exc_info()[1]
            print(e)

        # Se recibe el mensaje de vuelta
        msg_in = self.socket.recv()
        noEnviado, nombreServidor = msg_in.split(',')
        if int(noEnviado):
            devuelve = True
        else:
            devuelve = False
        return devuelve


def validaSms(num, msg):
    devuelve = True
    if not num:
        devuelve = False
    elif not msg:
        devuelve = False
    elif len(num) != 11:
        devuelve = False
    elif num[:4] not in ['0426', '0416', '0414', '0424', '0412']:
        devuelve = False

    # Esta Opcion es temporal para poder enviar yo Mensajes Internacionales
    usuario = bottle.request.get_cookie("account")
    if usuario == 'foxcarlos':
        devuelve = True

    return devuelve

app = enviarZMQ()
app.zmqCConectar()


@bottle.post('/mensaje')
def webService():
    '''
    Para el caso de que sea via Desktop
    import urllib2
    import urllib

    url = 'http://foxcarlos.no-ip.biz/mensaje'
    data = urllib.urlencode({'numero' : '04263002966', 'mensaje'  : 'Hola'})
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    response.read()

    Probar desde el Terminal en Linux
    curl -F "numero=04263002966" -F "mensaje=Hola" http://foxcarlos.no-ip.biz/mensaje
    '''

    # postdata = bottle.request.body.readline()

    l = []
    for f in sorted(bottle.request.POST, reverse=True):
        print(bottle.request.POST.get(f))
        l.append(bottle.request.forms.get(f))

    msg = 0
    numero, mensaje = l
    print(l)
    if validaSms(numero.strip(), mensaje.strip()):
        devuelve = app.enviar(numero, mensaje)
        print(devuelve)

        if devuelve:
            # cabecera = 'Felicidades ...'
            # msg = 'Mensaje enviado con exito'.format(numero)
            msg = 1
        else:
            # cabecera = 'Lo Siento ...!'
            # msg = 'No se pudo enviar el SMS al numero:{0}'.format(numero)
            msg = 0
    return template('{{msg}}', msg=msg)

bottle.run(host='0.0.0.0', port=80, server=GeventWebSocketServer, reloader=True)
